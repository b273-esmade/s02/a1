// Quiz
/*
1. Spaghetti code
2. let nameOfObject = {key:value}
3. Encapsulation
4. studentOne.enroll()/this.enroll()
5. True
6. {key:value}
7. True
8. True
9. True
10. True
*/


// Function Coding

//create student two
let studentTwoName = 'Joe';
let studentTwoEmail = 'joe@mail.com';
let studentTwoGrades = [78, 82, 79, 85];

//create student three
let studentThreeName = 'Jane';
let studentThreeEmail = 'jane@mail.com';
let studentThreeGrades = [87, 89, 91, 93];

//create student four
let studentFourName = 'Jessie';
let studentFourEmail = 'jessie@mail.com';
let studentFourGrades = [91, 89, 92, 93];




let studentOne = {
    name: 'John',
    email: 'john@mail.com',
    grades: [89, 84, 78, 88],

    login() {
        console.log(`${this.email} has logged in`);
    },
    logout() {
        console.log(`${this.email} has logged out`)
    },
    listGrades() {
       console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`); 
    },
    // Mini Exercise1
    	// compute average
    getAverage() {
    	let sum = 0
    	this.grades.forEach(grade => sum = sum + grade)
    	average=sum/4
    	return average;
    },
    // Mini Exercise2
    	// return true if average is >=85 otherwise false

    willPass(){
    	return this.getAverage() >=85 ? true : false
    },
    // Mini Exercise3
    	// return true if average is >=90 otherwise false
    willPassWithHonors(){
    	return (this.willPass() && this.getAverage() >=90) ? true : false
    }

}


let studentTwo = {
    name: 'Joe',
    email: 'joe@mail.com',
    grades:  [78, 82, 79, 85],

    login() {
        console.log(`${this.email} has logged in`);
    },
    logout() {
        console.log(`${this.email} has logged out`)
    },
    listGrades() {
       console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`); 
    },
    // returnAverage
    getAverage() {
    	let sum = 0
    	this.grades.forEach(grade => sum = sum + grade)
    	average=sum/4
    	return average;
    },
    // return true if average is >=85 otherwise false
    willPass(){
    	return this.getAverage() >=85 ? true : false
    },
   	// return true if average is >=90 otherwise false
    willPassWithHonors(){
    	return (this.willPass() && this.getAverage() >=90) ? true : false
    }
}


let studentThree = {
    name: 'Jane',
    email: 'jane@mail.com',
    grades:  [87, 89, 91, 93],

    login() {
        console.log(`${this.email} has logged in`);
    },
    logout() {
        console.log(`${this.email} has logged out`)
    },
    listGrades() {
       console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`); 
    },
    // returnAverage
    getAverage() {
    	let sum = 0
    	this.grades.forEach(grade => sum = sum + grade)
    	average=sum/4
    	return average;
    },
    // return true if average is >=85 otherwise false
    willPass(){
    	return this.getAverage() >=85 ? true : false
    },
   	// return true if average is >=90 otherwise false
    willPassWithHonors(){
    	return (this.willPass() && this.getAverage() >=90) ? true : false
    }
}


let studentFour = {
    name: 'Jessie',
    email: 'jessie@mail.com',
    grades: [91, 89, 92, 93],

    login() {
        console.log(`${this.email} has logged in`);
    },
    logout() {
        console.log(`${this.email} has logged out`)
    },
    listGrades() {
       console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`); 
    },
    // returnAverage
    getAverage() {
    	let sum = 0
    	this.grades.forEach(grade => sum = sum + grade)
    	average=sum/4
    	return average;
    },
    // return true if average is >=85 otherwise false
    willPass(){
    	return this.getAverage() >=85 ? true : false
    },
   	// return true if average is >=90 otherwise false
    willPassWithHonors(){
    	return (this.willPass() && this.getAverage() >=90) ? true : false
    }
}

// console.log(studentOne)
// console.log(studentTwo)
// console.log(studentThree)
// console.log(studentFour)



let classOf1A = {
	students: [studentOne,studentTwo,studentThree,studentFour],

//forEach()

	countHonorStudents(){
		let count = 0;
		this.students.forEach(student =>{
			if(student.willPassWithHonors()) count++
		})
		return count;
	},


// forLoop
	/*countHonorStudents(){
		let count = 0;
		for(let i = 0; i<this.students.length; i++) {
	        if (this.students[i].willPassWithHonors() == true){
	            count++;
	        }
	    }
	    return count;
	},*/


	honorsPercentage(){
		return (this.countHonorStudents()/this.students.length)*100;
		
	},

// forEach()

	retrieveHonorStudentInfo(){
		let honorStudents = [];
		this.students.forEach(student =>{
			if(student.willPassWithHonors()){
				honorStudents.push({
					name: student.name,
					email: student.email,
					average: student.getAverage()
				})
			}
		})
		return honorStudents;
	},


// forLoop
	/*retrieveHonorStudentInfo(){
		let honorStudents = [];
		for(let i = 0; i< this.students.length; i++){
			if(this.students[i].willPassWithHonors() == true){
				honorStudents.push(
					{name: this.students[i].name,
					email: this.students[i].email,
					average: this.students[i].getAverage()
					})
			}
		}
		return honorStudents;

	},*/



	sortHonorStudentsByGradeDesc(){
		let honorStudents = this.retrieveHonorStudentInfo()
		honorStudents.sort((a,b)=>{
			return b.average - a.average
		})
		return honorStudents;
	}



}


